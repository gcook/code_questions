﻿using System;
using System.Text;

namespace LinkedLists
{
    class LinkedList<T> { 
        public LinkedList()
        {
            Head = null;
            Curr = Head;
        }
        public Node<T> Head { get; private set; }
        public Node<T> Curr { get; private set; }
        public void Append(Node<T> node) {
            if (Head == null)
            {
                Head = node;
                Curr = Head;
            }
            else
            {
				MoveToEnd();
				Curr.Next = node;
				Curr = node;
			} 
        }
        public void PrintList() {
            if (Head == null) return;
            StringBuilder sb = new StringBuilder();
            Node<T> work = Head;
            do
            {
                sb.Append(work.Data).Append(",");
                work = work.Next;
            } while (work != null);

            var s = sb.ToString().TrimEnd(',');
            Console.WriteLine(s);
        }
        private void MoveToEnd()
        {
            if (Curr == null || Curr == Head || Curr.Next == null ) return;
            while (Curr.Next != null)
                Curr = Curr.Next;
        }

    }

    class Node<T> {
        public T Data { get; set; }
        public Node<T> Next { get; set; }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            var llist = new LinkedList<int>();
            var n = new Node<int>() { Data = 43 };
            llist.Append(n);
            llist.PrintList();
			llist.Append(new Node<int> { Data = 3 });
			llist.PrintList();
			llist.Append(new Node<int> { Data = 258});
			llist.PrintList();
			llist.Append(new Node<int> { Data = 17 });
			llist.PrintList();
		}
    }
}
