﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace stringManipulation
{
    class MainClass
    {
        static string Replace(string strIn, out int num_space) {
            num_space = 0;
            string ret_val = String.Empty;
            for (int i = 0; i < strIn.Length; i++) {
                if (strIn[i] == ' ')
                    num_space++;
                else
                    ret_val += strIn[i];
            }         
            return ret_val;
        }

        static string ReverseIgnoreSpecialChars(string strIn) {
            StringBuilder ret_val = new StringBuilder(strIn);
            int l = 0;
            int r = strIn.Length - 1;
            while (l < r )
            {
                if (!Char.IsLetterOrDigit(strIn[l]))
                    l++;
                else if (!Char.IsLetterOrDigit(strIn[r]))
                    r--;
                else {
                    ret_val[l] = strIn[r];
                    ret_val[r] = strIn[l];
                    l++;
                    r--;
                } 
            }
            return ret_val.ToString();
        }

        public static void Main(string[] args) {
            string testStr = "a!!!b.c.d,e'f,ghi";
			int count = 0;
            string s = Replace(testStr, out count);
            if (!String.IsNullOrEmpty(s))
            {
                // Count and remove blank spaces
                Console.WriteLine("String:{0}", testStr);
                Console.WriteLine("Replaced:{0}, Count:{1}", s, count);
                // Count and remove blank spaces immediate
                Console.WriteLine("Replaced:{0}, Count:{1}", testStr.Replace(" ", String.Empty), new Regex(@"\s").Matches(testStr).Count);
                testStr = " abc$d e||";
                s = ReverseIgnoreSpecialChars(testStr);
				Console.WriteLine("String: {0}", testStr);
				Console.WriteLine("Reversed: {0}", s);
			}
        }
    }
}
